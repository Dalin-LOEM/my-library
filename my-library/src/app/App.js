import React from 'react';
import { Router, Route, Link } from 'react-router-dom';
import './index.scss'
import {history} from '../_helpers/history';
import { authenticationService } from '../_services/authentication.service';
import { PrivateRoute } from '../_components/PrivateRoute';
import HomePage from '../home-page/index';
import { LoginPage } from '../login-page/LoginPage';
import ReactClass from '../page/category/react/React'
import ReactHooks from '../page/category/react-hooks/index'
import ReactApi from '../page/category/react-api/index'
import ReactHooksApi from '../page/category/react-api/ReactHook-Api'

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null
        };
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({ currentUser: x }));
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    render() {
        const { currentUser } = this.state;
        return (
            <Router history={history}>

                <div className="app">
                    {currentUser &&
                        <nav className="navbar navbar-expand navbar-dark bg-info">
                            <div className="navbar-nav">
                                <Link to="/" className="nav-item nav-link">Home</Link>
                                <Link to="/react-course" className="nav-item nav-link">React</Link>
                                <Link to="/react-hooks" className="nav-item nav-link">React Hooks</Link>
                                <Link to="/react-api" className="nav-item nav-link">React API</Link>
                                <Link to="/react-hooks-api" className="nav-item nav-link">ReactHooks & NodeJs</Link>
                                <a href onClick={this.logout} className="nav-item nav-link">Logout</a>
                            </div>
                        </nav>
                    }
                    <div className="row" style={{marginLeft:'15px', marginRight:'15px'}}>
                        <PrivateRoute exact path="/" component={HomePage} />
                        <Route path="/login" component={LoginPage} />
                        <Route path="/react-course" component={ReactClass} />
                        <Route path="/react-hooks" component={ReactHooks} />
                        <Route path="/react-api" component={ReactApi} />
                        <Route path="/react-hooks-api" component={ReactHooksApi} />
                    </div>  
                </div>
            </Router>
        );
    }
}

export default App;