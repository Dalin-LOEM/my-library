import React from 'react'
import './index.scss'
import ReackHook from './ReactHook'
import CountNum from './CountNum'
import HookApi from './HooksApi'

function App() {
    return(
        <div className="row">
            <ReackHook/>
            <CountNum/>
            <HookApi/>
        </div>
    )
}
export default App;