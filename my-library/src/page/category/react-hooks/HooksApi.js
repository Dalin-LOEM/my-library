import React, { useState, useEffect } from 'react';
import axios from 'axios';

function ListUser({index, data}) {
    return(
        <div>
            <li key={data.objectID}>
            <a href={data.url}>{data.title}</a>
            </li>
        </div>
    )
}

function App() {
  const [data, setData] = useState({ hits: [] });

  useEffect(async () => {
    const result = await axios(
      "https://hn.algolia.com/api/v1/search?query=redux",
    );
    setData(result.data);
  }, []);

  return (
        <div>
            <h4>Tutorialspoint</h4>
            {data.hits.map(item => (
                <ListUser 
                    data={item}
                />
            ))}
        </div>
  );
}
export default App;