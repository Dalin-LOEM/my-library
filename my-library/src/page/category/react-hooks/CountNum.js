import React , {useState, useEffect} from 'react'
import {Button} from 'antd'

function Todo({index, todo, updateText}) {
    return(
        <div className="todos">
            <p style={{color:'green'}}>{todo.text}</p>
            <p style={{color:'orange'}}>{todo.color}</p>
        </div>
    )
}
function CountNum() {
    const [count , setCount] = useState(0);
    const [todos, setTodos] = useState([
        { text: 'Learn Hooks' },
        { color: 'Orange' }
    ]);

    const updateText = index => {
        const newUpdate = [...todos];
        newUpdate[index].isUpdate = true;
        setTodos(newUpdate);
    }

    return(
        <div className="row" style={{marginTop:20, margin: 20}}>
            <div className="count">
                <p>Let's count {count}</p>
                <Button onClick={() =>setCount(count + 1)}>Click Me!</Button>
                <div className="hook" style={{marginTop:20}}>
                    {todos.map((todo, index) => (
                        <Todo
                            key={index}
                            index={index}
                            todo={todo}
                            updateText={updateText}
                        />
                    ))}
                </div>
            </div>
        </div>
    )
}
export default CountNum;