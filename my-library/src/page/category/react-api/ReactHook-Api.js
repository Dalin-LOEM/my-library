import React, {useState, useEffect} from 'react'
import axios from 'axios';

function List({item}) {
    return(
        <div>
            <p>{item.id}: {item.task} => {item.created_at}</p>
        </div>
    )
}

function ReactHooksApi() {

    const [data, setData] = useState([]);
    const apiUrl = "http://localhost:3000/tasks";

    useEffect(() => {
        const fetchData = async () => {
          const result = await axios(apiUrl);
          setData(result.data);
        };
      
        fetchData();
      }, []);

    return(
        <div>
            <h3>WELCOME TO REACTJS WITH NODEJS(MYSQL)</h3>
            {data.map(item => (
                <List 
                    item={item}
                />
            ))}
        </div>
    )
}
export default ReactHooksApi;