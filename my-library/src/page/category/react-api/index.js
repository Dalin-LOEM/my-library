import React from 'react' 
import axios from 'axios';

const apiEndpoint = "http://localhost:3000/tasks";

class ReactApi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           dataSource:[],
        };
     }

    async componentDidMount(){
        const { data: dataSource } = await axios.get(apiEndpoint);
        this.setState({ dataSource });
        console.log(dataSource);
    }
    
    render(){
        const {dataSource} = this.state
        return(
            <div>
                <h1>Hello API</h1>
                {dataSource.map(item => (
                    <p>{item.task}</p>
                ))}
            </div>
        )
    }
}

export default ReactApi;