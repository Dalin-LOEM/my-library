import React from 'react'

function ReactClass() {
    return(
        <div className="container">
            <h3>Welcome to React Course</h3>
        </div>
    )
}
export default ReactClass;